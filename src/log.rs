use std::fs::{File, OpenOptions};
use std::io::{BufWriter, Write};
use std::sync::Mutex;

use lazy_static::lazy_static;

lazy_static! {
    static ref LOG_FILE: Mutex<BufWriter<File>> = Mutex::new(BufWriter::new(
        OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open("log")
            .expect("Unable to create log file")
    ));
}

pub fn log(string: &str)
{
    println!("{}", string);

    let mut file = LOG_FILE.lock().unwrap();
    let _ = file.write(string.as_bytes());
    let _ = file.write(b"\n");
    let _ = file.flush();
}
