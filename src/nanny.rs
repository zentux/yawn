use xcb::x::{self, Screen};
use xcb::{randr, Connection, ProtocolError};

use crate::client::Client;
use crate::log;
use crate::monitor::Monitor;

/// You can trust her with your windows. Promise.
pub struct Nanny<'a>
{
    con:             &'a Connection,
    screen:          &'a Screen,
    monitors:        Vec<Monitor<'a>>,
    focused_monitor: usize,
}

impl<'a> Nanny<'a>
{
    pub fn new(con: &'a Connection, screen: &'a Screen) -> Result<Self, xcb::Error>
    {
        let mouse_c = con.send_request(&x::QueryPointer {
            window: screen.root(),
        });
        let mon_c = con.send_request(&randr::GetMonitors {
            window:     screen.root(),
            get_active: true,
        });
        let reply = con.wait_for_reply(mon_c)?;

        let monitors: Vec<Monitor> = reply
            .monitors()
            .map(|info| {
                Monitor::new(
                    con,
                    info.x(),
                    info.y(),
                    info.width() as u32,
                    info.height() as u32,
                )
            })
            .collect();

        for m in &monitors {
            log::log(&format!(
                "{}x{} pos: ({}, {})",
                m.width(),
                m.height(),
                m.x(),
                m.y()
            ));
        }

        let mut this = Self {
            con,
            screen,
            monitors,
            focused_monitor: 0,
        };

        // Focus the monitor the mouse pointer is currently on
        if let Ok(mouse) = con.wait_for_reply(mouse_c) {
            match this.first_monitor_containing(mouse.root_x(), mouse.root_y()) {
                Some(mon) => this.focused_monitor = mon,
                None => { /* Keep focused monitor at 0 */ },
            }
        }

        Ok(this)
    }

    pub fn add(&'a mut self, window: x::Window) -> Result<(), ProtocolError>
    {
        if !self.has_window(window) {
            self.focused_monitor_mut().add(window);
        }

        Ok(())
    }

    pub fn get_client(&self, window: x::Window) -> Option<&Client>
    {
        self.monitors.iter().find_map(|m| m.get_client(window))
    }

    pub fn get_client_mut(&mut self, window: x::Window) -> Option<&'a mut Client>
    {
        self.monitors
            .iter_mut()
            .find_map(|m| m.get_client_mut(window))
    }

    pub fn has_window(&self, window: x::Window) -> bool
    {
        self.monitors.iter().any(|m| m.has_window(window))
    }

    pub fn conn(&self) -> &Connection { self.con }

    pub fn on_mouse_move(&mut self, x: i16, y: i16)
    {
        if let Some(mon) = self.first_monitor_containing(x, y)
            && self.focused_monitor != mon
        {
            self.focused_monitor = mon;
        }
    }

    fn first_monitor_containing(&self, x: i16, y: i16) -> Option<usize>
    {
        self.monitors.iter().position(|mon| mon.contains(x, y))
    }

    pub fn focused_monitor(&self) -> &'a Monitor
    {
        self.monitors.get(self.focused_monitor).unwrap()
    }

    pub fn focused_monitor_mut(&mut self) -> &'a mut Monitor
    {
        self.monitors.get_mut(self.focused_monitor).unwrap()
    }

    /*
    pub fn add(&mut self, window: Window, attributes: XWindowAttributes)
    {
        // Only add the window if it is not already known
        if self.clients.iter().all(|c| window != c.window()) {
            self.clients
                .push(Client::new(&self.display, window, attributes));
            unsafe {
                XChangeProperty(
                    self.display.inner(),
                    self.display.default_root_window(),
                    self.display.net_atom(NetAtom::ClientList),
                    XA_WINDOW,
                    32,
                    PropModeAppend,
                    &window as *const u64 as *const u8,
                    1,
                );
                XMapWindow(self.display.inner(), window);
            }

            // XXX: Focusing the first client is just for a test
            self.get_client(window).unwrap().focus(self.display());

            self.arrange();
            unsafe {
                XSync(self.display.inner(), False);
            }
        }
    }

    pub fn arrange(&mut self)
    {
        // All Fullscreen windows for testing.
        for c in &mut self.clients {
            c.resize(&self.display, self.x, self.y, self.width, self.height);
        }
    }

    pub fn display(&self) -> &Display { &self.display } */
}
