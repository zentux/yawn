use core::slice;
use std::ffi::c_void;
use std::mem::MaybeUninit;

use xcb::x::{self, ConfigWindow, ConfigureWindow, Cw, EventMask, InputFocus, CURRENT_TIME};
use xcb::{Connection, ProtocolError};

use crate::log;

pub struct Client<'a>
{
    con:    &'a Connection,
    window: x::Window,
    x:      i16,
    y:      i16,
    width:  u32,
    height: u32,
}

impl<'a> Client<'a>
{
    pub fn new(
        con: &'a Connection,
        window: x::Window,
        x: i16,
        y: i16,
        width: u32,
        height: u32,
    ) -> Result<Self, ProtocolError>
    {
        let ac = con.send_request_checked(&x::ChangeWindowAttributes {
            window,
            value_list: &[Cw::EventMask(
                EventMask::ENTER_WINDOW
                    | EventMask::FOCUS_CHANGE
                    | EventMask::PROPERTY_CHANGE
                    | EventMask::STRUCTURE_NOTIFY,
            )],
        });

        let mc = con.send_request_checked(&x::MapWindow { window });

        con.check_request(ac)?;
        con.check_request(mc)?;

        let mut this = Self {
            con,
            window,
            x,
            y,
            width,
            height,
        };

        this.resize(x, y, width, height);

        Ok(this)
    }

    pub fn window(&self) -> x::Window { self.window }

    pub fn resize(
        &mut self,
        x: i16,
        y: i16,
        width: u32,
        height: u32,
    ) -> Result<(), xcb::ProtocolError>
    {
        self.con.send_and_check_request(&x::ConfigureWindow {
            window:     self.window,
            value_list: &[
                ConfigWindow::X(x as i32),
                ConfigWindow::Y(y as i32),
                ConfigWindow::Width(width),
                ConfigWindow::Height(height),
            ],
        })?;

        log::log(&format!(
            "Resizing client: at ({}, {}), size ({}, {})",
            x, y, width, height
        ));

        self.x = x;
        self.y = y;
        self.width = width;
        self.height = height;

        Ok(())
    }

    pub fn focus(&mut self)
    {
        self.con.send_request(&x::SetInputFocus {
            revert_to: InputFocus::None,
            focus:     self.window,
            time:      CURRENT_TIME,
        });
    }
}
