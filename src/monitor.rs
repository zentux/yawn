use xcb::{x, Connection};

use crate::client::Client;

pub struct Monitor<'a>
{
    con:     &'a Connection,
    x:       i16,
    y:       i16,
    width:   u32,
    height:  u32,
    clients: Vec<Client<'a>>,
}

impl<'a> Monitor<'a>
{
    pub fn new(con: &'a Connection, x: i16, y: i16, width: u32, height: u32) -> Self
    {
        Self {
            con,
            x,
            y,
            width,
            height,
            clients: Vec::new(),
        }
    }

    pub fn add(&mut self, window: x::Window)
    {
        // Create a client covering the entire monitor.
        self.clients
            .push(Client::new(self.con, window, self.x, self.y, self.width, self.height).unwrap())
    }

    pub fn remove(&mut self, window: x::Window)
    {
        // TODO: Short-circuit?
        self.clients.retain(|c| c.window() != window);
    }

    pub fn take(&mut self, window: x::Window) -> Option<Client>
    {
        for (i, c) in self.clients.iter().enumerate() {
            if c.window() == window {
                return Some(self.clients.swap_remove(i));
            }
        }

        None
    }

    pub fn get_client(&self, window: x::Window) -> Option<&Client>
    {
        self.clients.iter().find(|c| c.window() == window)
    }
    pub fn get_client_mut(&mut self, window: x::Window) -> Option<&'a mut Client>
    {
        self.clients.iter_mut().find(|c| c.window() == window)
    }

    pub fn has_window(&self, window: x::Window) -> bool
    {
        self.clients.iter().any(|c| c.window() == window)
    }

    /// Check if the monitor contains the given coordinates
    pub fn contains(&self, x: i16, y: i16) -> bool
    {
        x >= self.x
            && y >= self.y
            && x < self.x + self.width as i16
            && y < self.y + self.height as i16
    }

    pub fn x(&self) -> i16 { self.x }
    pub fn y(&self) -> i16 { self.y }
    pub fn width(&self) -> u32 { self.width }
    pub fn height(&self) -> u32 { self.height }
}
