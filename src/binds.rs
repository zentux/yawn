use std::process::Command;

use xcb::x::{self, GrabMode, KeyButMask, KeyPressEvent, ModMask, Screen};
use xcb::{Connection, ProtocolError};

pub struct Binds<'a>
{
    con:    &'a Connection,
    screen: &'a Screen,
}

impl<'a> Binds<'a>
{
    pub fn new(con: &'a Connection, screen: &'a Screen) -> Self
    {
        ungrab_all(con, screen.root()).unwrap();

        // Register a test binding
        con.send_request(&x::GrabKey {
            owner_events:  true,
            grab_window:   screen.root(),
            modifiers:     ModMask::N4 | ModMask::SHIFT,
            key:           36, // Return
            pointer_mode:  GrabMode::Async,
            keyboard_mode: GrabMode::Async,
        });

        Self { con, screen }
    }

    pub fn on_key_event(&self, ev: KeyPressEvent)
    {
        // Check if the modifier keys are pressed
        let mod_mask = KeyButMask::MOD4 | KeyButMask::SHIFT;
        if ev.state() ^ mod_mask == !mod_mask {
            // Ignore
            return;
        }

        if ev.detail() == 36 {
            // Meta + Shift + Return (open terminal)
            Command::new("alacritty")
                .spawn()
                .expect("Terminal could not be run");
        }
    }
}

impl<'a> Drop for Binds<'a>
{
    fn drop(&mut self) { ungrab_all(self.con, self.screen.root()).unwrap() }
}

fn ungrab_all(con: &Connection, window: x::Window) -> Result<(), ProtocolError>
{
    con.send_and_check_request(&x::UngrabKey {
        key:         0, // XCB_GRAB_ANY (in this case means ungrab all)
        grab_window: window,
        modifiers:   ModMask::ANY,
    })
}
