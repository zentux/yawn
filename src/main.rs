#![feature(let_chains)]

pub mod atoms;
pub mod binds;
pub mod client;
pub mod log;
pub mod monitor;
pub mod nanny;

use std::process::Command;

use atoms::Atoms;
use binds::Binds;
use nanny::Nanny;
use xcb::x::{ClientMessageEvent, EventMask};
use xcb::{x, Connection, Event, Xid};

fn main()
{
    log::log("Good morning. Yawn.");

    let (conn, screen_num) = Connection::connect(None).expect("Unable to connect to X-Server");

    let setup = conn.get_setup();
    let screen = setup.roots().nth(screen_num as usize).unwrap();

    let atoms = Atoms::intern_all(&conn).expect("Unable to create atoms");

    // Check if another window manager is running
    let c = conn.send_request_checked(&x::ChangeWindowAttributes {
        window:     screen.root(),
        value_list: &[x::Cw::EventMask(
            EventMask::SUBSTRUCTURE_REDIRECT
                | EventMask::SUBSTRUCTURE_NOTIFY
                | EventMask::STRUCTURE_NOTIFY
                | EventMask::PROPERTY_CHANGE,
        )],
    });
    if let Err(_err) = conn.check_request(c) {
        log::log("Another window manager is already running. I thought you trusted me :/");
        return;
    }

    /*
    conn.send_and_check_request(&x::SendEvent {
        propagate:   false,
        destination: x::SendEventDest::Window(screen.root()),
        event_mask:  EventMask::STRUCTURE_NOTIFY,
        event:       &ClientMessageEvent::new(
            screen.root(),
            atoms.manager,
            x::ClientMessageData::Data32([
                x::CURRENT_TIME,
                atoms.manager.resource_id(),
                screen.root().resource_id(),
                0,
                0,
            ]),
        ),
    })
    .expect("Unable to send wm notify event");
    */

    let mut nanny = Nanny::new(&conn, &screen).expect("Unable to create window manager");
    let binds = Binds::new(&conn, &screen);

    loop {
        let ev = match conn.wait_for_event() {
            Ok(ev) => ev,
            Err(xcb::Error::Protocol(_)) => {
                continue;
            },
            Err(xcb::Error::Connection(_)) => {
                log::log("Connection closed by X-Server");
                break;
            },
        };

        match ev {
            Event::X(x::Event::MapRequest(map_request)) => on_map_request(&mut nanny, map_request),
            Event::X(x::Event::UnmapNotify(unmap_notify)) => {
                on_unmap_notification(&mut nanny, unmap_notify)
            },
            // Event::X(x::Event::ConfigureRequest(conf_request)) => on_conf_request(&mut nanny,
            // conf_request),
            Event::X(x::Event::KeyPress(key_press)) => binds.on_key_event(key_press),
            Event::X(x::Event::MotionNotify(motion_notify)) => {
                nanny.on_mouse_move(motion_notify.root_x(), motion_notify.root_y())
            },
            o => log::log(&format!("Event not handled: {:?}", o)),
        }
    }

    log::log("Yawn is tired. Shutting down.");
}

fn on_map_request<'a, 'b>(nanny: &'a mut Nanny<'b>, map_request: x::MapRequestEvent)
where
    'a: 'b,
{
    let window = map_request.window();

    // Get the window attributes
    let c = nanny
        .conn()
        .send_request(&x::GetWindowAttributes { window });

    if nanny.has_window(window) {
        return;
    }

    if let Ok(r) = nanny.conn().wait_for_reply(c)
        && !r.override_redirect()
    {
        let _ = nanny.add(window);
    }
}

fn on_unmap_notification(nanny: &mut Nanny, unmap_notify: x::UnmapNotifyEvent)
{
    nanny.remove(unmap_notify.window());
}
